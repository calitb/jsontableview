//
//  JSONTableViewController.m
//  JSONTableView
//
//  Created by Carlos Thurber B. on 03/05/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import "JSONTableViewController.h"
#import "UISwitchAccessory.h"
#import "DetailViewControllerProtocol.h"

#ifndef Log
#define Log(...) NSLog((@"%s [Line %d] "), __PRETTY_FUNCTION__, __LINE__); \
NSLog(__VA_ARGS__); \
printf("\n");
#endif

#define EQUALS(a,b) [a isEqualToString:b]

@interface JSONTableViewController ()

@end

@implementation JSONTableViewController

- (NSString*) tableStructureFileName{
    if (_tableStructureFileName==nil) {
        return @"test1.json";
    }
    else {
        return _tableStructureFileName;
    }
}

- (NSDictionary*) tableStructure{
    if (_tableStructure==nil) {
        NSString *path = [[NSBundle mainBundle] pathForResource:self.tableStructureFileName ofType:@""];
        NSData *content = [NSData dataWithContentsOfFile:path];
        NSError *error = nil;
        _tableStructure = [NSJSONSerialization JSONObjectWithData:content options:kNilOptions error:&error];

        if (error!=nil) {
            Log(@"%@",error.localizedDescription);
        }
        
    }
    
    return _tableStructure;
    
}

- (void)viewDidLoad
{
    self.title = self.tableStructure[@"viewcontroller_title"];

    UIBarButtonItem *backbutton = [[UIBarButtonItem alloc] initWithTitle:@"Volver" style:UIBarButtonItemStylePlain target:nil action:nil];
    self.navigationItem.backBarButtonItem = backbutton;
    
    [super viewDidLoad];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.tableStructure[@"table"][section][@"header_title"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.tableStructure[@"table"] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableStructure[@"table"][section][@"rows"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *jsonInfo = self.tableStructure[@"table"][indexPath.section][@"rows"][indexPath.row];
    Log(@"DATA %@",jsonInfo.description);
    
    NSString *cellIdentifier = jsonInfo[@"cellIdentifier"];
    cellIdentifier = cellIdentifier==nil?@"basic":cellIdentifier;
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    //textos
    cell.textLabel.text = jsonInfo[@"text"];
    cell.detailTextLabel.text = jsonInfo[@"detail"];
    
    //color de selección de la celda
    id action = jsonInfo[@"action"];
    NSString *selectionStyle = jsonInfo[@"selectionStyle"];
    if (EQUALS(selectionStyle, @"UITableViewCellSelectionStyleNone")) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if (EQUALS(selectionStyle, @"UITableViewCellSelectionStyleGray")) {
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    }
    else if (EQUALS(selectionStyle, @"UITableViewCellSelectionStyleBlue")) {
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    else if (action!=nil){
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    else {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    //accesory
    NSString *accessoryType = jsonInfo[@"accessoryType"];
    NSString *switch_pref = jsonInfo[@"switch_pref"];
    if (EQUALS(accessoryType, @"UITableViewCellAccessoryDisclosureIndicator")) {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (EQUALS(accessoryType, @"UITableViewCellAccessoryDisclosureSwitch") && switch_pref!=nil) {
        UISwitchAccessory *switchview = [[UISwitchAccessory alloc] initWithFrame:CGRectZero];
        switchview.indexPath = indexPath;
        switchview.on = [[NSUserDefaults standardUserDefaults] boolForKey:switch_pref];
        [switchview addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchview;
    }
    else if (EQUALS(accessoryType, @"UITableViewCellAccessoryNone")) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if (action!=nil){
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
    
}

#pragma mark - Table view delegate

//sólo si tiene action se permite seleccionar la celda
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *jsonInfo = self.tableStructure[@"table"][indexPath.section][@"rows"][indexPath.row];
    id action = jsonInfo[@"action"];
    return action!=nil?indexPath:nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *jsonInfo = self.tableStructure[@"table"][indexPath.section][@"rows"][indexPath.row];
    Log(@"DATA %@",jsonInfo.description);
    id action = jsonInfo[@"action"];
    
    
    if ([action respondsToSelector:@selector(length)]) {
        NSString *actionString = action;
        
        //se carga otra tabla
        if ([actionString rangeOfString:@".json"].location != NSNotFound) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
            JSONTableViewController *vc = (JSONTableViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"JSONTableViewController"];
            vc.tableStructureFileName = actionString;
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
    //se carga otro viewcontroller
    else if ([action respondsToSelector:@selector(objectForKey:)]) {
        NSDictionary *data = [action objectForKey:@"data"];
        NSString *transition = [action objectForKey:@"transition"];
        
        Class class = NSClassFromString([action objectForKey:@"class"]);
        
        UIViewController <DetailViewControllerProtocol> *vc = [[class alloc] init];
        vc.data = data;
        
        if (EQUALS(transition, @"push")) {
            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
}

#pragma mark -

-(void)switchValueChanged:(id)sender
{
    UISwitchAccessory *swView = (UISwitchAccessory*)sender;
    NSIndexPath *indexPath = swView.indexPath;
    NSDictionary *jsonInfo = self.tableStructure[@"table"][indexPath.section][@"rows"][indexPath.row];
    NSString *switch_pref = jsonInfo[@"switch_pref"];
    
    BOOL oldValue = [[NSUserDefaults standardUserDefaults] boolForKey:switch_pref];
    [[NSUserDefaults standardUserDefaults] setBool:!oldValue forKey:switch_pref];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

@end
