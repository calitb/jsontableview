//
//  JSONTableViewController.h
//  JSONTableView
//
//  Created by Carlos Thurber B. on 03/05/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JSONTableViewController : UITableViewController

@property (nonatomic, retain) NSDictionary *tableStructure;
@property (nonatomic, retain) NSString *tableStructureFileName;

@end


/*

 "table" : [
 [
 {
 "header_title":"Este es el titulo",
 "rows":
 [
 {
 "accessoryType":"UITableViewCellAccessoryDisclosureIndicator",
 "detail":"some text",
 "text":"some text",
 "cellIdentifier" : "subtitle",
 "selectionStyle" : "blue"
 
 "action":"another.json",
 "image_name" : "something",
 "image_url" : "something",
 "switch_pref" : "pref"
 }
 ]
 }
 ]
 "tableViewStyle" : "UITableViewStylePlain",
 "viewcontroller_title" : "my title"
 }
 
 
text: texto para el textLabel
detail: texto para el detailTextLabel
accessoryType: UITableViewCellAccessoryDisclosureIndicator, UITableViewCellAccessorySwitch (debe definirse switch_pref), UITableViewCellAccessoryNone. Si no se define es UITableViewCellAccessoryNone, pero si tiene action es UITableViewCellAccessoryDisclosureIndicator
selectionStyle: UITableViewCellSelectionStyleNone, UITableViewCellSelectionStyleGray, UITableViewCellSelectionStyleBlue. Si no se define es UITableViewCellSelectionStyleNone, pero si tiene action es UITableViewCellSelectionStyleBlue
cellIdentifier: basic, subtitle, left_detail, right_detail. Si no se define es basic
switch_pref: preferencia booleana donde almacenar el valor del switch
viewcontroller_title: titulo del viewcontroller
tableViewStyle (NO SOPORTADO): UITableViewStylePlain, UITableViewStyleGrouped
header_title: titulo de la sección

 action: 
 A) nombre del archivo json a cargar en otra tabla
 B) diccionario con data, class y transition definidos. Transition sólo puede ser push. La clase debe implementar el protocolo DetailViewControllerProtocol
         {
         "class" : "DetailViewController",
         "data" : { "titulo" : "es una prueba" }
         "transition" : "push"
         }
 
 

 
 
 action: json, selector (debe recibir el NSIndexPath), dictionary con data y clase, push-modal (se carga la clase que indica, esta clase debe soportar la propiedad data, protocolo y ser un viewcontroller. Si es modal debe soportar la propiedad modalDelegate)

 
*/