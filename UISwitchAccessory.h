//
//  UISwitchAccessory.h
//  JSONTableView
//
//  Created by Carlos Thurber B. on 03/05/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISwitchAccessory : UISwitch

@property (nonatomic, retain) NSIndexPath *indexPath;

@end
