//
//  UISwitchAccessory.m
//  JSONTableView
//
//  Created by Carlos Thurber B. on 03/05/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import "UISwitchAccessory.h"

@implementation UISwitchAccessory

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
