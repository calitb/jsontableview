//
//  DetailViewControllerProtocol.h
//  JSONTableView
//
//  Created by Carlos Thurber B. on 03/05/13.
//  Copyright (c) 2013 MindsLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DetailViewControllerProtocol <NSObject>

@property (nonatomic, retain) NSDictionary* data;

@end